﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;


public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int x = Request.Browser.ScreenPixelsWidth;
        int y = Request.Browser.ScreenPixelsHeight;

        int c1;
        int c2;


        GridViewDealing.DataSource = ((DAO)Application["DAO"]).Dealing;
        GridViewDealing.DataBind();

        GridViewLifebroker.DataSource = ((DAO)Application["DAO"]).Lifebroker;
        GridViewLifebroker.DataBind();

        c1 = GridViewDealing.Columns.Count;
        c2 = GridViewLifebroker.Columns.Count;

       /* for (int i = 0; i < c1; i++)
        {
            GridViewDealing.Columns[i].ItemStyle.Width = (int)(x * 0.95 / 10);
        }

        for (int i = 0; i < c2; i++)
        {
            GridViewLifebroker.Columns[i].ItemStyle.Width = (int)(x * 0.95 / 10);
        }*/


        GridViewDealing.Font.Size = (int)(y / 10);
        GridViewLifebroker.Font.Size = (int)(y / 10);
        GridViewDealing.Height = (int)(y * 0.4);
        GridViewLifebroker.Height = (int)(y * 0.4);

        Label1.Font.Size = (int)(y * 0.05);
        Label2.Font.Size = (int)(y * 0.05);
        Label3.Font.Size = (int)(y * 0.05);

        Label1.Text = ((DAO)Application["DAO"]).Flag.ToString();
    }

    
}
