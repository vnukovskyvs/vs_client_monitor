﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Timers;
using System.Data;
using Sybase.Data.AseClient;

/// <summary>
/// Summary description for DAO
/// </summary>
public class DAO
{

    private AseConnection conn;
    private AseConnection conn_2;
    private AseDataReader dr;
    private AseDataReader dr_2;
    private AseCommand cmd;
    private AseCommand cmd_2;


    private DataTable dt;
    private DataTable dt_2;

    private DataTable dt_old;
    private DataTable dt_2old;

    private Timer timer = new Timer(500);

    private int flag;

    public DAO()
    {
        conn = new AseConnection("Data Source='172.16.21.20';Port=5000;Database='front_dealing';Uid='tv_user';Pwd='tv_user'");
        conn_2 = new AseConnection("Data Source='172.16.21.20';Port=5000;Database='life_broker';Uid='tv_user';Pwd='tv_user'");
    /*    conn = new AseConnection("Data Source='172.16.21.19';Port=5000;Database='front_dealing_test';Uid='tv_user';Pwd='tv_user'");
        conn_2 = new AseConnection("Data Source='172.16.21.19';Port=5000;Database='life_broker_test';Uid='tv_user';Pwd='tv_user'");*/


        dt = new DataTable();
        dt_2 = new DataTable();

        timer.Enabled = true;
        timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
        timer.Start();

        flag = 1;
        dt_old = new DataTable();
        dt_2old = new DataTable();
    }

    public int Flag
    {
        get
        {
            return flag;
        }
    }

    public DataTable Dealing
    {
        get
        {
            return dt;
        }
    }

    public DataTable Lifebroker
    {
        get
        {
            return dt_2;
        }
    }

    void timer_Elapsed(object sender, ElapsedEventArgs e)
    {
        timer.Interval = 600000;
        //timer.Interval = 6000;

        try
        {
                conn.Open();

            cmd = new AseCommand("web_ask_itogo", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            dr = cmd.ExecuteReader();

            dt_old.Clear();
            dt_old = dt.Copy();

            dt.Clear();

            dt.Load(dr);

            dr = cmd.ExecuteReader();

            conn.Close();
        }
        catch (AseException ex)
        {
            string er =  ex.Source + " : " + ex.Message + " (" +   ex.ToString() + ")";
        }

        try
        {
            conn_2.Open();

            cmd_2 = new AseCommand("web_ask_itogo", conn_2);
            cmd_2.CommandType = System.Data.CommandType.StoredProcedure;
            dr_2 = cmd_2.ExecuteReader();

            dt_2old.Clear();
            dt_2old = dt_2.Copy();

            dt_2.Clear();

            dt_2.Load(dr_2);

            //dr_2 = cmd_2.ExecuteReader();
            //GridView2.DataSource = dr2;
            //GridView2.DataBind();
            //conn_2.Close();
        }
        catch (AseException ex)
        {
            string er = ex.Source + " : " + ex.Message + " (" +  ex.ToString() + ")";
        }

        /*
                dr2 = cmd2.ExecuteReader();
                dr = cmd.ExecuteReader();
         */

        flag = 0;

        try
        {
            if (dt != null && dt_old != null)
            {
                if (dt.Columns.Count == dt_old.Columns.Count && dt.Rows.Count == dt_old.Rows.Count)
                {
                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        for (int b = 0; b < dt.Columns.Count; b++)
                        {
                            if (string.Compare(dt.Rows[a][b].ToString(), dt_old.Rows[a][b].ToString()) != 0)
                            {
                                flag = 1;
                            }
                        }
                    }
                }
                else
                {
                    flag = 1;
                }
            }

            if (dt_2 != null && dt_2old != null)
            {
                if (dt_2.Columns.Count == dt_2old.Columns.Count && dt_2.Rows.Count == dt_2old.Rows.Count)
                {
                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        for (int b = 0; b < dt.Columns.Count; b++)
                        {
                            if (string.Compare(dt_2.Rows[a][b].ToString(), dt_2old.Rows[a][b].ToString()) != 0)
                            {
                                flag = 1;
                            }
                        }
                    }
                }
                else
                {
                    flag = 1;
                }
            }
        }
        catch (Exception ex)
        {
            string er = ex.Source + " : " + ex.Message + " (" + ex.ToString() + ")";
        }

    }


}